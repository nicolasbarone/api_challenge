FROM  openjdk:17
WORKDIR /apichallenge
ADD build/libs/*.jar apichallenge.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "apichallenge.jar"]