# ApiChallenge 

## Requirements
- Java 17
- IntelliJ
- Docker Desktop
- Postman

## Getting started

Esta aplicación es una simple REST Full Api con algunos endpoints de prueba y posee auntenticacion mediante JWT. 
Para eso es necesario primero registrarse y luego loguearse, de ahi en mas el token se ira propagando en los demas 
request de la coleccion de postman en la variable de entorno "token" definida en el ambiente local de postman. 
El token tiene un expiration configurado y parametrizado en 5 minutos. 
La API posee encriptación de password al momento de registrarse y se valida al momento del Logueo del usuario. 
Utliza una BD PostgreSql para almacenar los datos de Usuario, Suscripciones y Pagos, implementado con Jpa+Hibernate, 
utilizando como payment gateway Stripe para el manejo de Suscripciones y Pagos.
Como aclaracion, se genero una cuenta en stripe, con los 2 productos (Anual - $90 y Mensual - $9.99); 
con los siguiente datos para podes acceder y visualizar el dashboard:

```
mail: apichallenge2024@gmail.com
pass: apichallenge.2024
```

Una vez descargado el proyecto desde git, ejecutar el build de gradle y luego desde la terminal ejecutamos el 
docker compose para levantar las 3 imagenes que utiliza la api (La api en si, la imagen de la bd postgre y el CLI de stripe):

```
    docker compose up -d
```

Una vez que se levantas las imagenes, entramos a la imagen de Stripe y en la solapa de CLI ejecutamos:
```
    stripe login
```
Esto nos retorna una URL que accedemos desde el browser para concedernos acceso; nos pedira estar logueado en el 
dashboard con las claves especificadas mas arriba.

***


# Description
 Para realizar una prueba completa, como primer paso deberiamos registrarnos o crear un nuevo usuario:
 ```
curl --location 'http://localhost:8080/api/user/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "alberto",
    "password": "123456",
    "name": "alberto",
    "lastname": "stripe",
    "email": "alberto@gmail.com"
}'
```
luego usamos el Login para poder obtener el token y poder consumir el resto de endpoints:
 ```
curl --location 'http://localhost:8080/api/user/login' \
--header 'Content-Type: application/json' \
--data '{
    "username": "alberto",
    "password": "123456"
}'
```
Procedemos a ejecutar el getAll de los Tipos de Suscripciones disponibles (No requiere Token) para usar posteriormente 
en la alta:
```
curl --location 'http://localhost:8080/api/subscription/getAll'
```
Y finalmente generamos la alta de la Suscripcion para el usuario que creamos con el id de la suscripcion que elijamos 
este endpoint esta securizo, por lo tanto hay que enviar el token JWT generado:
```
curl --location 'http://localhost:8080/api/subscription/new' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer xxxxxxxxxxxx' \
--data '{
"idPrice" : "idPrice_xxxxxxxxxxxxxx",
"idUser" : "idUser_xxxxxxxxxxxxxxx",
"successUrl": "https://google.com.ar/",
"cancelUrl": "https://google.com.ar/"
}'
```

Este ultimo endpoint nos devolvera una URL para realizar el pago de ls suscripcion, para poder finalizar correctamente
se debe llenar el campo de la tarje con los caracteres: 
```
Nro de Tarjeta:    4242 4242 4242 4242
Fecha Vto:   04/24            CVC: 424
```
Al seleccionar suscribirse, se realiza el pago y la suscripcion en Stripe (Pudiendo visualizarlo desde el dashboard);
esto genera un evento en Stripe lo cual es notificado en el webhook de la api, el cual termina impactando los datos de
suscripcion y pago en la BD.


## Authors
Nicolas Barone 
