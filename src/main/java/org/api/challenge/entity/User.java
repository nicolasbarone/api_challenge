package org.api.challenge.entity;

import lombok.Data;
import jakarta.persistence.*;
import org.apache.commons.lang3.StringUtils;

@Data
@Entity
@Table(name = "usuario")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String name;
    private String lastname;
    private String email;
    private String idStripeCustomer;
    private boolean newsletters;

    public User(){
    }

    public boolean isUsernameAndPasswordPresent() {
        return !StringUtils.isAnyEmpty(username, password);
    }


    public String getUsername() {
        return this.username;
    }

    public void setPassword(String encodedPassword) {
        this.password = encodedPassword;
    }

    public String getPassword() {
        return this.password;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName(){
        return name + " " + lastname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIdStripeCustomer() {
        return idStripeCustomer;
    }

    public void setIdStripeCustomer(String idStripeCustomer) {
        this.idStripeCustomer = idStripeCustomer;
    }

    public boolean isNewsletters() {
        return newsletters;
    }

    public void setNewsletters(boolean newsletters) {
        this.newsletters = newsletters;
    }
}
