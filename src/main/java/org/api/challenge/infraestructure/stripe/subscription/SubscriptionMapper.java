package org.api.challenge.infraestructure.stripe.subscription;

import com.stripe.model.Price;
import org.api.challenge.delivery.dto.SubscriptionTypeDTO;

import java.math.BigDecimal;

public class SubscriptionMapper {

    public static SubscriptionTypeDTO convertToDto(Price price){

        SubscriptionTypeDTO dto = new SubscriptionTypeDTO();

        dto.setId(price.getId());
        dto.setDescription(price.getNickname());
        dto.setPrice(price.getUnitAmountDecimal().divide(BigDecimal.valueOf(100)));
        dto.setCurrency(price.getCurrency());
        return dto;
    }
}
