package org.api.challenge.infraestructure.stripe.subscription;

import com.google.gson.JsonSyntaxException;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import com.stripe.net.ApiResource;
import com.stripe.net.Webhook;
import com.stripe.param.PriceListParams;

public class StripeSubscriptionUtil {

    public static PriceCollection getAllSubscription() throws StripeException {
        PriceListParams params = PriceListParams.builder().setLimit(3L).build();
        PriceCollection prices = Price.list(params);

        return prices;
    }

    public static StripeObject readSubscriptionFromEvent(Event event) throws Exception {


        // Deserialize the nested object inside the event
        EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
        StripeObject stripeObject = null;
        if (dataObjectDeserializer.getObject().isPresent()) {
            return dataObjectDeserializer.getObject().get();
        } else {

            throw new Exception("----------No se pudo leer el evento");
        }
    }


    public static Event readEvent(String endpointSecret, String sigHeader, String payload) throws Exception {
        Event event = null;

        if (endpointSecret != null && sigHeader != null) {
            // Only verify the event if you have an endpoint secret defined.
            // Otherwise use the basic event deserialized with GSON.
            try {
                event = Webhook.constructEvent(payload, sigHeader, endpointSecret);
            } catch (SignatureVerificationException e) {
                // Invalid signature
                System.out.println("⚠️  Webhook error while validating signature.");
            }
        }

        if(event == null) {
            try {
                event = ApiResource.GSON.fromJson(payload, Event.class);
            } catch (JsonSyntaxException e) {
                // Invalid payload
                System.out.println("⚠️  Webhook error while parsing basic request.");
                throw new Exception("----------No se pudo leer el evento");
            }
        }
        return event;
    }
}
