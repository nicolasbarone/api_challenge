package org.api.challenge.infraestructure.repository;

import org.api.challenge.entity.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    Subscription findByIdStripeSubscription(String idStripeSubscription);
}
