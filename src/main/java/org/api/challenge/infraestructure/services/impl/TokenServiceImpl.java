package org.api.challenge.infraestructure.services.impl;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.api.challenge.delivery.dto.UserDTO;
import org.api.challenge.entity.User;
import org.api.challenge.infraestructure.services.TokenService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class TokenServiceImpl implements TokenService {

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.secret}")
    private String secret;

    @Override
    public String generateToken(User user) {
        Instant expirationTime = Instant.now().plus(expiration, ChronoUnit.MINUTES);
        Date expirationDate = Date.from(expirationTime);

        Key key = Keys.hmacShaKeyFor(secret.getBytes());

        String compactTokenString = Jwts.builder()
                .claim("id", user.getId())
                .claim("username", user.getUsername())
                .claim("name", user.getName())
                .claim("lastname", user.getLastname())
                .setExpiration(expirationDate)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();

        return "Bearer " + compactTokenString;
    }

    @Override
    public UserDTO parseToken(String token) {

        byte[] secretBytes = secret.getBytes();

        try {
            Jws<Claims> jwsClaims = Jwts.parserBuilder()
                    .setSigningKey(secretBytes)
                    .build()
                    .parseClaimsJws(token);

            Claims body = jwsClaims.getBody();

            String username = body.getSubject();
            Long userId = body.get("id", Long.class);
            String name = body.get("name", String.class);
            String lastname = body.get("lastname", String.class);

            return new UserDTO(userId, name, lastname, username);
        } catch (Exception ex){
            throw new JwtException("Usuario no autentificado o Token Expirado");
        }
    }
}
