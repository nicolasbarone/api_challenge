package org.api.challenge.infraestructure.services.impl;

import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import org.apache.commons.lang3.StringUtils;
import org.api.challenge.application.exception.UserNotFoundException;
import org.api.challenge.application.exception.UsernameAlreadyExistsException;
import org.api.challenge.application.exception.UsernameOrPasswordInvalidException;
import org.api.challenge.entity.User;
import org.api.challenge.infraestructure.repository.UserRepository;
import org.api.challenge.infraestructure.services.TokenService;
import org.api.challenge.infraestructure.services.UserService;
import org.api.challenge.infraestructure.stripe.customer.StripeCustomerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    private BCryptPasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private TokenService tokenService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passEncoder, TokenService tokenService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passEncoder;
        this.tokenService = tokenService;
    }

    @Override
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }


    @Override
    public User save(User user) {
        if (user.isUsernameAndPasswordPresent()) {
            if (!userRepository.existsByUsername(user.getUsername())) {
                try {

                    Customer customer = StripeCustomerUtil.createCustomer(user.getEmail(), user.getFullName());
                    user.setIdStripeCustomer(customer.getId());
                    String encodedPassword = this.passwordEncoder.encode(user.getPassword());
                    user.setPassword(encodedPassword);
                    return this.userRepository.save(user);
                } catch (StripeException ex) {
                    ex.printStackTrace();
                    throw new UsernameAlreadyExistsException();
                }
            }
            throw new UsernameAlreadyExistsException();
        }
        throw new UsernameOrPasswordInvalidException();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public boolean existsById(Long userId) {
        return userRepository.existsById(userId);
    }

    @Override
    public String login(String username, String password) {
        if (!StringUtils.isAllBlank(username, password)) {
            User user = findByUsername(username);
            if (Objects.nonNull(user) && passwordEncoder.matches(password, user.getPassword())) {
                return tokenService.generateToken(user);
            }
        }
        throw new UsernameOrPasswordInvalidException();
    }

}
