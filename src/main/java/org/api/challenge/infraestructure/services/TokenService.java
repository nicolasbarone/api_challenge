package org.api.challenge.infraestructure.services;


import org.api.challenge.delivery.dto.UserDTO;
import org.api.challenge.entity.User;

public interface TokenService {
    String generateToken(User user);
    UserDTO parseToken(String token);
}