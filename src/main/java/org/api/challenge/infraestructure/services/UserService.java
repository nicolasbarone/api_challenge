package org.api.challenge.infraestructure.services;

import org.api.challenge.entity.User;
import java.util.List;

public interface UserService {
    User findById(Long id);
    User save(User user);
    User findByUsername(String username);
    String login(String username, String password);
    boolean existsById(Long userId);

}
