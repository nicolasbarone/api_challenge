package org.api.challenge.infraestructure.services.impl;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.Event;
import com.stripe.model.Price;
import com.stripe.model.StripeObject;
import org.api.challenge.entity.Payment;
import org.api.challenge.entity.Subscription;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;
import jakarta.annotation.PostConstruct;
import org.api.challenge.application.exception.UserNotFoundException;
import org.api.challenge.delivery.dto.SubscriptionRequestDTO;
import org.api.challenge.delivery.dto.SubscriptionTypeDTO;
import org.api.challenge.entity.User;
import org.api.challenge.infraestructure.repository.PaymentRepository;
import org.api.challenge.infraestructure.repository.SubscriptionRepository;
import org.api.challenge.infraestructure.repository.UserRepository;
import org.api.challenge.infraestructure.services.SubscriptionService;
import org.api.challenge.infraestructure.stripe.customer.StripeCustomerUtil;
import org.api.challenge.infraestructure.stripe.subscription.StripeSubscriptionUtil;
import org.api.challenge.infraestructure.stripe.subscription.SubscriptionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    @Autowired
    private PaymentRepository paymentRepository;
    @Value("${stripe.secret}")
    private String secret;
    @Value("${stripe.webhook.secret}")
    private String endpointSecret;
    @PostConstruct
    public void init() {
        Stripe.apiKey = secret;
    }
    @Override
    public List<SubscriptionTypeDTO> getAllSubscriptionType() {
        List<SubscriptionTypeDTO> result = new ArrayList<>();
        try {
            StripeSubscriptionUtil.getAllSubscription().getData()
                    .stream()
                    .forEach(e -> result.add(SubscriptionMapper.convertToDto(e)));
        } catch (StripeException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public String create(SubscriptionRequestDTO requestDTO) {

        Optional<User> userOp = userRepository.findById(requestDTO.getIdUser());

        Customer customer = null;
        try {
            customer = StripeCustomerUtil.findCustomer(userOp.orElseThrow( ()-> new UserNotFoundException()).getEmail());

            SessionCreateParams params = SessionCreateParams.builder()
                    .addLineItem(
                            SessionCreateParams.LineItem.builder().setPrice(requestDTO.getIdPrice()).setQuantity(1L).build())
                    .setCustomer(customer.getId())
                    .setMode(SessionCreateParams.Mode.SUBSCRIPTION)
                    .setSuccessUrl(requestDTO.getSuccessUrl())
                    .setCancelUrl(requestDTO.getCancelUrl())
                    .build();
            Session session = Session.create(params);
            return session.getUrl();
        } catch (StripeException e) {
            throw new RuntimeException(e);
        }
    }

    public void registerChangesSubscription(String payload, String stripeSignature){
        Event event = null;
        StripeObject stripeObject = null;
        try {
            event = StripeSubscriptionUtil.readEvent(endpointSecret, stripeSignature, payload);
        // Handle the event

            stripeObject = StripeSubscriptionUtil.readSubscriptionFromEvent(event);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        com.stripe.model.Subscription stripeSubscription = null;
        switch (event.getType()) {
            case "customer.subscription.created":
                stripeSubscription = (com.stripe.model.Subscription) stripeObject;
                Subscription subscriptionEntity = subscriptionRepository.findByIdStripeSubscription(stripeSubscription.getId());
                if(subscriptionEntity == null) {
                    subscriptionEntity = createSubscription(stripeSubscription);
                }else{
                    subscriptionEntity.setExpirationDate(new Date(stripeSubscription.getCurrentPeriodEnd()*1000));
                }

                createPayment(subscriptionEntity, stripeSubscription.getItems().getData().get(0).getPrice());
            default:
                System.out.println("Unhandled event type: " + event.getType());
        }

    }

    private Subscription createSubscription(com.stripe.model.Subscription stripeSubscription){
        Subscription subscription = new Subscription();

        User user = userRepository.findByIdStripeCustomer(stripeSubscription.getCustomer());
        subscription.setUser(user);
        subscription.setDate(new Date(stripeSubscription.getCurrentPeriodStart()*1000));
        subscription.setExpirationDate(new Date(stripeSubscription.getCurrentPeriodEnd()*1000));
        subscription.setIdStripeSubscription(stripeSubscription.getId());
        subscription.setStatus(stripeSubscription.getStatus());

        return subscriptionRepository.save(subscription);
    }

    private void createPayment(Subscription subscription, Price price){
        Payment payment = new Payment();
        payment.setSubscription(subscription);
        payment.setCurrency(price.getCurrency());
        payment.setAmount(price.getUnitAmountDecimal().divide(BigDecimal.valueOf(100)));
        payment.setDate(new Date(price.getCreated()*1000));

        paymentRepository.save(payment);
    }
}
