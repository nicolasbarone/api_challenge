package org.api.challenge.infraestructure.services;

import org.api.challenge.delivery.dto.SubscriptionRequestDTO;
import org.api.challenge.delivery.dto.SubscriptionTypeDTO;

import java.util.List;

public interface SubscriptionService {

    List<SubscriptionTypeDTO> getAllSubscriptionType();
    String create(SubscriptionRequestDTO requestDTO);
    void registerChangesSubscription(String payload, String stripeSignature);
}
