package org.api.challenge.application.exception;


public class UsernameAlreadyExistsException extends AlreadyExistsException {
    public UsernameAlreadyExistsException() {
        super("Username");
    }
}
