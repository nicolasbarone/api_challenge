package org.api.challenge.application.exception;

public class AlreadyExistsException extends IllegalArgumentException {
    public AlreadyExistsException(String entityName) {
        super(entityName + " already exists");
    }
}
