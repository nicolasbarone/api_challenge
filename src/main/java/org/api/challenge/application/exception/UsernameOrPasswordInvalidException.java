package org.api.challenge.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class UsernameOrPasswordInvalidException extends IllegalArgumentException {
    public UsernameOrPasswordInvalidException() {
        super("Username or password invalid");
    }
}
