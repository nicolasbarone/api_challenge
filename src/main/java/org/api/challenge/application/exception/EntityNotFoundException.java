package org.api.challenge.application.exception;

public class EntityNotFoundException extends IllegalArgumentException {
    public EntityNotFoundException(String entityName) {
        super(entityName + " with the specified id does not exists");
    }
}
