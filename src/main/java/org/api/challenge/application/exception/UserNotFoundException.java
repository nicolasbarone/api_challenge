package org.api.challenge.application.exception;

public class UserNotFoundException extends EntityNotFoundException {
    public UserNotFoundException() {
        super("User");
    }
}
