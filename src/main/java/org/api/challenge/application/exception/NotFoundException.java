package org.api.challenge.application.exception;

public class NotFoundException extends Exception {
    public NotFoundException(String uri) {
        super(uri + " does not exist");
    }
}
