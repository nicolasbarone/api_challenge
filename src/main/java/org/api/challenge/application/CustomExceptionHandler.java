package org.api.challenge.application;

import org.api.challenge.application.exception.AlreadyExistsException;
import org.api.challenge.application.exception.EntityNotFoundException;
import org.api.challenge.application.exception.NotFoundException;
import org.api.challenge.application.exception.UsernameOrPasswordInvalidException;
import org.api.challenge.delivery.dto.ErrorDTO;
import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    @ResponseBody
    public ResponseEntity<?> requestHandlingNoHandlerFound() {
        return generateResponse(new NotFoundException("URI"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<?> notFoundExceptionResolver(EntityNotFoundException exc) {
        return generateResponse(exc, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {AlreadyExistsException.class})
    public ResponseEntity<?> alreadyExistsException(AlreadyExistsException exc) {
        return generateResponse(exc, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {IllegalAccessException.class})
    public ResponseEntity<?> illegalAccessException(IllegalAccessException exc) {
        return generateResponse(exc, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {UsernameOrPasswordInvalidException.class})
    public ResponseEntity<?> usernameOrPasswordInvalidException(UsernameOrPasswordInvalidException exc) {
        return generateResponse(exc, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {ServiceException.class})
    public ResponseEntity<?> serviceException(ServiceException exc) {
        return generateResponse(exc, HttpStatus.SERVICE_UNAVAILABLE);
    }

    /*
    @ExceptionHandler(value = {JwtException.class})
    public ResponseEntity<?> expiredJwtException(JwtException exc) {
        return generateResponse(exc, HttpStatus.FORBIDDEN);
    }
    */

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public Map<String,String> missingField(MethodArgumentNotValidException exc) {
        Map<String,String> errors = new HashMap<>();
        exc.getBindingResult().getAllErrors().forEach( (error) -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        return errors;
    }

    private static ResponseEntity<ErrorDTO> generateResponse(Exception exc, HttpStatus httpStatus) {
        ErrorDTO errorDTO = new ErrorDTO(exc);
        return new ResponseEntity<>(errorDTO, httpStatus);
    }
}
