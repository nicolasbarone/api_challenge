package org.api.challenge.utils;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.stream.Collectors;


public class GenericMapper<T, V> {

    private ModelMapper modelMapper;
    private Type entityType;
    private Type dtoType;

    public GenericMapper(Type entityType, Type dtoType) {
        this.modelMapper = new ModelMapper();
        this.modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        this.entityType = entityType;
        this.dtoType = dtoType;
    }

    public V convertToDTO(T entity) {
        return modelMapper.map(entity, this.dtoType);
    }

    public T convertToEntity(V dto) {
        return modelMapper.map(dto, this.entityType);
    }

    public Collection<V> convertToDTO(Collection<T> entities) {
        return entities.stream().map(this::convertToDTO).collect(Collectors.toList());
    }
}
