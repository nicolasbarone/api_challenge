package org.api.challenge.delivery.controller;


import jakarta.validation.Valid;
import org.api.challenge.delivery.dto.ResponseUserDTO;
import org.api.challenge.delivery.dto.TokenDTO;
import org.api.challenge.delivery.dto.UserDTO;
import org.api.challenge.delivery.dto.UserLoginDTO;
import org.api.challenge.entity.User;
import org.api.challenge.infraestructure.services.UserService;
import org.api.challenge.utils.GenericMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {


    private final UserService userService;
    private GenericMapper<User, UserDTO> mapperReq;
    private GenericMapper<User, ResponseUserDTO> mapperResp;

    public UserController(UserService userService) {
        this.userService = userService;
        this.mapperReq = new GenericMapper<>(User.class, UserDTO.class);
        this.mapperResp = new GenericMapper<>(User.class, ResponseUserDTO.class);
    }

    @PostMapping("/login")
    public TokenDTO login(@RequestBody @Valid UserLoginDTO userLoginDTO) {
        String token = userService.login(userLoginDTO.getUsername(), userLoginDTO.getPassword());
        return new TokenDTO(token);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseUserDTO singUpUser(@RequestBody @Valid UserDTO userDTO) {
        User userSaved = this.userService.save(mapperReq.convertToEntity(userDTO));
        return mapperResp.convertToDTO(userSaved);
    }


    @GetMapping()
    public ResponseUserDTO findById() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDTO user = (UserDTO) authentication.getPrincipal();
        return mapperResp.convertToDTO(userService.findById(user.getId()));
    }

}
