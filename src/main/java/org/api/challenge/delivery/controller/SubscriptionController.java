package org.api.challenge.delivery.controller;

import com.stripe.exception.StripeException;
import org.api.challenge.delivery.dto.SubscriptionRequestDTO;
import org.api.challenge.delivery.dto.SubscriptionTypeDTO;
import org.api.challenge.infraestructure.services.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscription")
public class SubscriptionController {

    @Autowired
    SubscriptionService subscriptionService;
    @GetMapping("/getAll")
    public List<SubscriptionTypeDTO> findAllUsers() {
        return subscriptionService.getAllSubscriptionType();
    }

    @PostMapping("/new")
    String newSubscription(@RequestBody SubscriptionRequestDTO requestDTO) throws StripeException {
        return subscriptionService.create(requestDTO);
    }

    @PostMapping("/webhook")
    String notification(@RequestBody String requestJson, @RequestHeader(value="Stripe-Signature") String stripeSignature) throws StripeException {
        subscriptionService.registerChangesSubscription(requestJson, stripeSignature);
        return "ok";
    }

}
