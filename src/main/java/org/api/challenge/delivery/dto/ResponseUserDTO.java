package org.api.challenge.delivery.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import jakarta.validation.constraints.NotBlank;


@Data
@AllArgsConstructor
public class ResponseUserDTO {
    private Long id;
    @NotBlank
    private String username;
    private String name;
    private String lastname;
    private String email;
    private boolean newsletters;

    public ResponseUserDTO(){}

    public ResponseUserDTO(Long id, String username, String name, String lastname) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.lastname = lastname;
    }
}
