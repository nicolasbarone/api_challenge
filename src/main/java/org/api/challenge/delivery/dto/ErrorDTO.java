package org.api.challenge.delivery.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonPropertyOrder({"message", "dateTime"})
public class ErrorDTO {
    private String message;
    private LocalDateTime dateTime;

    public ErrorDTO(Exception exc) {
        this.message = exc.getMessage();
        this.dateTime = LocalDateTime.now();
    }
}
